# Auth0 Flutter Web

A wrapper for Auth0 SPA SDK to use Auth0 in Flutter Web.

> ⚠ The latest is [`0.0.1-alpha.6`](https://pub.dev/packages/auth0_flutter_web/versions/0.0.1-alpha.6) but not serve by `pub.dev` by default.
>
> Please note:  previous version in use with Cloudline Solutions was based on  [`0.0.1-alpha.3`](https://pub.dev/packages/auth0_flutter_web/versions/0.0.1-alpha.3) 

[![](https://img.shields.io/pub/v/auth0_flutter_web)](https://pub.dev/packages/auth0_flutter_web)

## How to use

### 1. Add dependency

  ```yaml
  auth0_flutter_web:
      git:
        url: https://bitbucket.org/parachute360/p360_flutter_auth0_null_safe.git
  ```

### 2. Load Auth0 SPA SDK in `index.html`

   Make Auth0 SPA SDK available for dart by adding it to `index.html` in `web` folder (besides `ios` and `android`)

  ```HTML
  <head>
    <!-- other elements in head -->
    <script src="https://cdn.auth0.com/js/auth0-spa-js/1.13/auth0-spa-js.production.js"></script>
    <!-- other elements in head -->
  </head>
  ```

### 3. Use `Auth0` in dart code

  ```dart
  import 'package:auth0_flutter_web/auth0_flutter_web.dart';

   await dotenv.load(fileName: "config.env");

   Auth0 auth0client = await createAuth0Client(Auth0CreateOptions(
        domain: dotenv.env['AUTH0_DOMAIN'] ?? '',       // not nullable string 
        client_id: dotenv.env['AUTH0_CLIENT_ID'] ?? '', // not nullable string
        audience: dotenv.env['AUTH0_AUDIENCE'] ?? '',   // not nullable string
        redirect_uri: redirectURI,                      // not nullable string
   ));

  Map<String, dynamic>? user = await auth0client.getUserDataOrLoginWithRedirect();

  ```

## 

